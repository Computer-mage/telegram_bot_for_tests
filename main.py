from config import TOKEN, DBWork
import telebot
from telebot import types
import sqlite3, json

db = DBWork()
bot = telebot.TeleBot(TOKEN);

def BdCheck(cursor):   
    cursor.execute(db.CREATETABLEQUESTIONS)
    cursor.execute(db.CREATETABLEANSWER)
    cursor.execute(db.CREATETABLEUSERS)
    conn.commit()
    conn.close()

@bot.message_handler(commands=['start'])
def start_test(message):
    conn = sqlite3.connect("Questions.db")
    cursor = conn.cursor()
    uid = message.from_user.id
    cursor.execute(f"INSERT OR IGNORE INTO Users (UserId) VALUES({uid})")
    conn.commit()
    conn.close()
    bot.send_message(message.from_user.id, "Тивирп")


@bot.message_handler(commands=['question'])
def get_question(message):
    conn = sqlite3.connect("Questions.db")
    cursor = conn.cursor()
    markup = types.InlineKeyboardMarkup()
    markup.row_width = 2
    cursor.execute(f"INSERT OR IGNORE INTO Users (UserId) VALUES({message.from_user.id})")
    sql = f"SELECT QuestionsText FROM Questions WHERE id=(SELECT CurentQuestion FROM Users WHERE UserId='{message.from_user.id}')"
    sql2 = f"SELECT AnswerText, QuestionId, id FROM Answers WHERE QuestionId=(SELECT id FROM Questions WHERE id=(SELECT CurentQuestion FROM Users WHERE UserId='{message.from_user.id}'))"
    cursor.execute(sql)
    qtext = cursor.fetchall()[0][0]
    cursor.execute(sql2)
    for ans in cursor.fetchall():
        dt = {
            "user_id": message.from_user.id, 
            "qid": ans[1],
            "aid": ans[2],
        }
        button1 = types.InlineKeyboardButton(ans[0], callback_data = json.dumps(dt))
        markup.add(button1)
    bot.send_message(message.from_user.id, qtext, reply_markup=markup)
    conn.commit()
    conn.close()

@bot.callback_query_handler(func=lambda call: True) 
def callback_inline(call): 
    if call.data:
        if call.data.startswith("Question"):
            get_question(call)
        else:
            d = json.loads(call.data)
            conn = sqlite3.connect("Questions.db")
            cursor = conn.cursor()
            markup = types.InlineKeyboardMarkup()
            qid = d["qid"]
            aid = d["aid"]
            sql2 = f"SELECT AnswerText FROM Answers  WHERE id={aid}"
            sql = f"SELECT CorectAnswerId FROM Questions Where Id={qid}"
            cursor.execute(sql2)
            atext = cursor.fetchall()[0][0]
            cursor.execute(sql)
            truornot = "Верно" if cursor.fetchall()[0][0] == aid else "Неверно"
            if truornot == "Верно":
                nqid = int(qid)+1
                cursor.execute(f"SELECT id FROM Questions WHERE id={nqid}")
                if cursor.fetchall()!=[]:
                    cursor.execute(f"UPDATE Users SET CurentQuestion={nqid}")
                else:
                    cursor.execute("UPDATE Users SET CurentQuestion=1")
                button1 = types.InlineKeyboardButton("Дальше", callback_data = "Question")
                markup.add(button1)
            atext2 = f"Ваш ответ: {atext} и это {truornot}"
            bot.send_message(d["user_id"], atext2, reply_markup=markup)
            conn.commit()
            conn.close()

if __name__ == "__main__":
    conn = sqlite3.connect("Questions.db")
    cursor = conn.cursor()
    BdCheck(cursor)
    bot.polling(none_stop=True, interval=0)