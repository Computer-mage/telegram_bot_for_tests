import sqlite3
TOKEN = "TokenPlaceHear"

class DBWork:
    CREATETABLEQUESTIONS = """
    CREATE TABLE IF NOT EXISTS "Questions" (
	    "id"	INTEGER,
	    "QuestionsText"	TEXT,
	    "CorectAnswerId"	INTEGER NOT NULL,
	    PRIMARY KEY("id" AUTOINCREMENT)
    );
    """
    CREATETABLEANSWER = """
    CREATE TABLE IF NOT EXISTS "Answers" (
	    "id"	INTEGER,
	    "AnswerText"	TEXT,
	    "QuestionId"	INTEGER,
	    PRIMARY KEY("id" AUTOINCREMENT)
    )
    """
    CREATETABLEUSERS="""
    CREATE TABLE IF NOT EXISTS "Users" (
	    "id"	INTEGER,
	    "UserId"	TEXT UNIQUE,
	    "CurentQuestion"	INTEGER DEFAULT 1,
	    PRIMARY KEY("id" AUTOINCREMENT)
    );
    """